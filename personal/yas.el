(require 'yasnippet)

(setq yas-snippet-dirs
      (append yas-snippet-dirs '("~/.emacs.d/personal/snippets")))

(yas-global-mode 1)

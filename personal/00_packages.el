(prelude-require-packages '(go-mode
                            ido-vertical-mode
                            mmm-mode
                            smart-mode-line
                            powerline
                            solarized-theme
                            switch-window
                            company
                            puppet-mode
                            yasnippet
                            smart-mode-line
                            dsvn
                            helm-ag))

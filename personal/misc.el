(global-set-key (kbd "C-c b") 'windmove-left)
(global-set-key (kbd "C-c f") 'windmove-right)
(global-set-key (kbd "C-c p") 'windmove-up)
(global-set-key (kbd "C-c n") 'windmove-down)

(require 'switch-window)
(global-set-key (kbd "C-x o") 'switch-window)

(require 'paren)
(setq show-paren-style 'parenthesis)
(show-paren-mode +1)

(global-hl-line-mode -1)

(setq auto-save-default nil)
